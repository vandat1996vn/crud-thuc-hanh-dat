import React from "react";
import "./App.css";
import TodoApp from "../src/pages/todo-app/TodoApp";
import CrudAxiosExample from "./pages/crud-axios-example/crud-axios-example";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { show: true };
  }

  render() {
    return (
      <>
        <TodoApp />
        <CrudAxiosExample/> 
      </>
    );
  }
}

export default App;
