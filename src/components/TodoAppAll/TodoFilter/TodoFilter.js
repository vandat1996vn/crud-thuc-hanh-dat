import React, { Component } from "react";

export default class TodoFilter extends Component {
  render() {
    return (
      <footer className="footer">
        <span className="todo-count">
          <strong>1</strong>
          <span> </span>
          <span>item</span>
          <span> left</span>
        </span>
        <ul className="filters">
          <li>
            <p
              className="selected"
              onClick={() => {
                this.props.handleFilterClick()
              }}
            >
              All
            </p>
          </li>
          <span> </span>
          <li>
            <p
              className=""
              onClick={() => {
                this.props.handleFilterClick(!this.props.isActives)
              }}
            >
              Active
            </p>
          </li>
          <span> </span>
          <li>
            <p
              className=""
              onClick={() => {
                this.props.handleFilterClick(this.props.isActives)
              }}
            >
              Completed
            </p>
          </li>
        </ul>
      </footer>
    );
  }
}
