import React, { Component } from "react";
import "./TodoApp.css";
import TodoFilter from "../../components/TodoAppAll/TodoFilter/TodoFilter";

export default class TodoApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listTodo: [
        {
          id: 1,
          content: "ăn sáng",
          isActive: false,
          isComplete: true,
        },
        {
          id: 2,
          content: "thay quần áo",
          isActive: false,
          isComplete: true,
        },
        {
          id: 3,
          content: "ghé thăm Mr.Wick",
          isActive: false,
          isComplete: true,
        },
      ],
      content: "",
      isActives: "",
      currentItem: "",
      isActive: "",
      isComplete: "",
    };
  }

  handleFilterClick = (isActives) => {
    this.setState((prevState) => ({
      ...prevState,
      listTodo: this.state.listTodo.filter((it) => {
        if (!isActives) {
          return it.isActive === false;
        } else if (isActives) {
          return it.isActive === true;
        } else {
          return it;
        }
      }),
    }));
    console.log(this.state.listTodo);
  };

  handleCreateTodo = (event) => {
    const { value } = event.target;
    if (event.key === "Enter") {
      event.stopPropagation();
      console.log(this.state.listTodo);
      this.setState((prevState) => ({
        ...prevState,
        listTodo: [
          ...prevState.listTodo,
          {
            id: 10,
            content: value,
            isActive: false,
            isComplete: true,
          },
        ],
      }));
    }
  };

  handleDelete = (item) => {
    this.setState((prevState) => ({
      ...prevState,
      listTodo: this.state.listTodo.filter((it) => {
        if (it.id !== item.id) {
          return it;
        }
      }),
    }));
  };

  handleActiveItem = (event) => {
    if (event.target.checked === true) {
      this.setState((prevState) => ({
        currentEntry: { ...event },
        ...prevState,
        listTodo: [
          ...prevState.listTodo.map((it) => {
            if (it.id === prevState.currentItem.id) {
              return { ...it, isActive: true, isComplete: false };
            }
            return it;
          }),
        ],
      }));
    } else {
      this.setState((prevState) => ({
        currentEntry: { ...event },
        ...prevState,
        listTodo: [
          ...prevState.listTodo.map((it) => {
            if (it.id === prevState.currentItem.id) {
              return { ...it, isActive: false, isComplete: true };
            }
            return it;
          }),
        ],
      }));
    }
    console.log(this.state.listTodo);
  };

  handleClickItem = (item) => {
    this.setState((prevState) => ({
      ...prevState,
      currentItem: { ...item },
    }));
  console.log(this.state.currentItem);
  };

  render() {
    return (
      <section className="todoapp">
        <div>
          <header className="header">
            <h1>todos</h1>
            <input
              className="new-todo"
              placeholder="What needs to be done?"
              type="text"
              value={this.content}
              onKeyDown={this.handleCreateTodo}
            />
          </header>
          <div className="main">
            <input id="toggle-all" className="toggle-all" type="checkbox" />
            <label htmlFor="toggle-all" />
            <ul className="todo-list">
              {this.state.listTodo.map((item) => {
                return (
                  <li className="" onClick={() => this.handleClickItem(item)}>
                    <div className="view">
                      <input
                        className="toggle"
                        type="checkbox"
                        onChange={this.handleActiveItem}
                      />
                      <label>{item.content}</label>
                      <button
                        className="destroy"
                        onClick={() => this.handleDelete(item)}
                      />
                    </div>
                    <input className="edit" defaultValue="ăn sáng" />
                  </li>
                );
              })}
            </ul>
          </div>
          <TodoFilter handleFilterClick={this.handleFilterClick} />
        </div>
      </section>
    );
  }
}
