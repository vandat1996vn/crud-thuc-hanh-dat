import React, { Component } from "react";
import Header from "../../components/CrudAxoisExample/Header/Header";
import "./CrudAxiosExample.css";
import EntryItem from "../../components/CrudAxoisExample/EntryItem/EntryItem";
import EntryModal from "../../components/EntryModal/EntryModal";

export default class CrudAxiosExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listEntries: [
        {
          id: 1,
          avatar:
            "https://static.wikia.nocookie.net/omniversal-battlefield/images/1/1e/Saber.%28Fate.stay.night%29.full.1044734.jpg/revision/latest?cb=20171117175924",
          description: "ahihihihi",
        },
        {
          id: 2,
          avatar:
            "https://i.pinimg.com/564x/86/f0/21/86f021b9fec389e1fcac518e05b8fc6b.jpg",
          description: "ahihihihi",
        },
        {
          id: 3,
          avatar:
            "https://static.wikia.nocookie.net/omniversal-battlefield/images/1/1e/Saber.%28Fate.stay.night%29.full.1044734.jpg/revision/latest?cb=20171117175924",
          description: "ahihihihi",
        },
        {
          id: 4,
          avatar:
            "https://i.pinimg.com/564x/86/f0/21/86f021b9fec389e1fcac518e05b8fc6b.jpg",
          description: "ahihihihi",
        },
        {
          id: 5,
          avatar:
            "https://static.wikia.nocookie.net/omniversal-battlefield/images/1/1e/Saber.%28Fate.stay.night%29.full.1044734.jpg/revision/latest?cb=20171117175924",
          description: "ahihihihi",
        },
        {
          id: 6,
          avatar:
            "https://i.pinimg.com/564x/86/f0/21/86f021b9fec389e1fcac518e05b8fc6b.jpg",
          description: "ahihihihi",
        },
      ],
      isShowModal: false,
      avatar: "",
      isCreateNew: true,
      currentEntry: "",
    };
  }

  handleSubmitEntry = () => {
    if (this.state.isCreateNew) {
      this.setState((prevState) => ({
        ...prevState,
        listEntries: [
          ...prevState.listEntries,
          {
            id: 10,
            avatar: prevState.avatar,
          },
        ],
        avatar: "",
      })); // thêm  : CREATE
    } else {
      this.setState((prevState) => ({
        ...prevState,
        listEntries: [
          ...prevState.listEntries.map((it) => {
            if (it.id === prevState.currentEntry.id) {
              return { ...it, avatar: prevState.avatar };
            }
            return it;
          }),
        ],
        avatar: "",
        isCreateNew: false,
      })); // sửa / UPDATE
    }
  };


  handleShowModal = () => {
    this.setState((prevState) => ({
      ...prevState,
      isShowModal: true,
    }));
  };
  // show modal

  handleHideModal = () => {
    this.setState((prevState) => ({
      ...prevState,
      isShowModal: false,
      isCreateNew: true,
    }));
  };
  // hide modal

  handleClick = (item) => {
    // console.log("item", item);
    this.setState((prevState) => ({
      // set lại state của item
      ...prevState, // rải vào trạng thái state hiện tại
      listEntries: this.state.listEntries.filter((it) => {
        if (it.id !== item.id) {
          return it;
        }
      }),
      // lọc ra các element có id khác với id của element được click nhận lại vào mảng listEntries
    }));
  };

  handleChangeAvatar = (event) => {
    this.setState((prevState) => ({
      ...prevState,
      avatar: event.target.value,
    }));
  };
  // thay ảnh va nhập value cho ảnh để  thêm item mới


  handleClickEntryItem = (item) => {
    this.setState((prevState) => ({
      ...prevState,
      avatar: item.avatar,
      isCreateNew: false,
      currentEntry: { ...item },
    }));
  };
  // show data của item lên modal

  render() {
    return (
      <>
        <Header handleShowModal={this.handleShowModal} />
        <main className="wrap_list" id="wrap_list">
          {this.state.listEntries.map((item) => {
            return (
              <EntryItem
                key={item.id}
                avatar={item.avatar}
                handleClick={() => this.handleClick(item)}
                handleShowModal={this.handleShowModal}
                handleClickEntryItem={() => this.handleClickEntryItem(item)}
              />
            );
          })}
        </main>
        {this.state.isShowModal && (
          <EntryModal
            handleHideModal={this.handleHideModal}
            avatar={this.state.avatar}
            isCreateNew={this.state.isCreateNew}
            handleChangeAvatar={this.handleChangeAvatar}
            handleSubmitEntry={this.handleSubmitEntry}
          />
        )}
      </>
      // nếu isShowModal = true thì render component <EntryModal/> và truyền vào các props là các  hàm và state là avatar
    );
  }
}
